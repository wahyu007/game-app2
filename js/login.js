let btnSubmit = document.getElementById("submit");
let alertInfo = document.getElementById("alert");
let token = localStorage.getItem("accessToken");
alertInfo.classList.add("hide");


if (!token) {
    console.log("accesToken is null")
} else {
    window.location.href = "index.html";
}

btnSubmit.addEventListener('click', () => {
    login();
});


let login = () => {
    let dataUSer = {
        "email" : document.getElementById("email").value,
        "password" : document.getElementById("password").value
    }

    fetch("http://localhost:8080/api/v1/users/login",{
        headers : {
            "content-type" : "application/json; charset=UTF-8"
        },
        method : 'POST',
        body : JSON.stringify(dataUSer)
    })
    .then(res => res.json())
    .then(data => {
        if (data.status == "success") {
            localStorage.setItem("accessToken", data.tokenAccess);
            window.location.href = "index.html";
        } else {
            console.log("back to login.html");
            alertInfo.innerHTML="Wrong email or password!";
            alertInfo.classList.remove("hide");
        }})
    .catch(err => {
        console.log(err);
        alertInfo.innerHTML="ERR_CONNECTION_REFUSED PLEASE TRY LATER";
        alertInfo.classList.remove("hide");
    } )
}

let checkUser = () => {
    fetch("http://localhost:8080/api/v1/users/profile",{
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then(res => res.json())
    .then(data => {
        if (data.status == "success") {
            window.location.href = "index.html";
        } else {
            
        }})
    .catch(err => {
        console.log(err);
    } )
}

btnSubmit.addEventListener('click', () => {
    register();
});

checkUser();