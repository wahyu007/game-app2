let name = document.getElementById('name');
let email = document.getElementById('email');
let password = document.getElementById('password');
let phone = document.getElementById('phone');
let address = document.getElementById('address');
let sex = document.getElementById('sex');

let btnSubmit = document.getElementById("register");
let alertInfo = document.getElementById("alert");
alertInfo.classList.add("hide");

let register = () => {
    let dataUser = {
        "name" : name.value,
        "email" : email.value,
        "password" : password.value,
        "phone" : phone.value,
        "address" : address.value,
        "sex" : sex.value
    }

    fetch("http://localhost:8080/api/v1/users/signup",{
        headers : {
            "content-type" : "application/json; charset=UTF-8"
        },
        method : 'POST',
        body : JSON.stringify(dataUser)
    })
    .then(res => res.text())
    .then(teks => {
        window.location.href = "login.html";
    })
    .catch(err => console.log(err));
}


let checkUser = () => {
    fetch("http://localhost:8080/api/v1/users/profile",{
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then(res => res.json())
    .then(data => {
        if (data.status == "success") {
            window.location.href = "index.html";
        } else {
            
        }})
    .catch(err => {
        console.log(err);
    } )
}

btnSubmit.addEventListener('click', () => {
    register();
});

checkUser();