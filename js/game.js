let accessToken = localStorage.getItem("accessToken");
let login = document.getElementById("login");
let infoLogin = document.getElementById("infoLogin");
let profile = document.getElementById("profile");
let register = document.getElementById("register"); 
let logout = document.getElementById("logout");


let removeSession = () => {
    localStorage.removeItem("accessToken");
    logoutMode();
}

let checkUser = () => {
    fetch("http://localhost:8080/api/v1/users/profile",{
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then(res => res.json())
    .then(data => {
        if (data.status == "success") {
            loginMode(data);
        }else {
            localStorage.removeItem("accessToken");
        }})
    .catch(err => {
        console.log(err);
    } )
}

let loginMode = (dataUser) => {
    document.getElementById("userName").innerHTML = dataUser.data.name
    infoLogin.classList.add("hide");
    profile.classList.remove("hide");
}

let logoutMode = () => {
    infoLogin.classList.remove("hide");
    profile.classList.add("hide");
}

logout.addEventListener("click", function() {
    removeSession()
});

checkUser();