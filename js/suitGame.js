let h1_player = document.getElementsByClassName("suit_player");
let h1_com = document.getElementsByClassName("suit_com");
let btnReset = document.getElementById("btnReset");
let txtInfo = document.getElementById("vs");
let txtScorePlayer = document.getElementsByClassName("score_player")[0];
let txtScoreCom = document.getElementsByClassName("score_com")[0];
let logout = document.getElementById("logout");
let picked_com;
let score_player = 0;
let score_com = 0;
let array_jakepon = ["batu", "kertas", "gunting"];
let beep_sound = "assets/beep.mp3";
let refresh_sound = "assets/negatif.mp3";

let beepSound = (src) => {
    let sound = document.createElement("audio");
    sound.src = src;
    sound.play();
  }

let random_suit = () => { 
    let i = Math.floor(Math.random() * 3);
    console.log("com" + ': ' + array_jakepon[i]);
    return i;
}

let picked = () => {
    for (let i = 0; i < h1_player.length; i++) {
        h1_player[i].addEventListener("click", event => {
            refresh();
            beepSound(beep_sound);

            picked_com = random_suit();
            console.log("player" + ': ' +array_jakepon[i]);

            h1_player[i].classList.add("kotak_picked");
            h1_com[picked_com].classList.add("kotak_picked");
            h1_com[picked_com].classList.add("rotate-hover");

            if (tos(array_jakepon[i], array_jakepon[picked_com]) == "win") {
                count_score("win");
            } else if(tos(array_jakepon[i], array_jakepon[picked_com]) == "lose") {
                count_score("lose");
            } else {
                count_score("draw");
            }
        });
    }
    reset();
}

let count_score = async(result) => {
    let score;
    if (result == "win") {
        score = 3;
        console.log("result : win");
        score_player++;
        txtScorePlayer.innerHTML = score_player;
        console.log("player : " + score_player);
        txtInfo.innerHTML = "Win !";
    } else if (result == "lose") {
        score = 0;
        console.log("result : lose");
        txtInfo.innerHTML = "Lose !";
        score_com++;
        txtScoreCom.innerHTML = score_com;
        console.log("com : " + score_com);
    } else {
        score = 1;
        console.log("result : draw");
        txtInfo.innerHTML = "Draw !";
    }
    await addScore(score);
    picked_com = random_suit();
} 

let refresh = () => {
    for( let i = 0; i < h1_player.length; i++){
        h1_player[i].classList.remove("kotak_picked");
        h1_com[i].classList.remove("kotak_picked");
        h1_com[i].classList.remove("rotate-hover");
    }
}

let reset = () => {
    btnReset.addEventListener("click", async event => {
        let resultGame;
        console.log("reset clicked");
        for (let i = 0; i < h1_player.length; i++) {
            h1_player[i].classList.remove("kotak_picked");
            h1_com[i].classList.remove("kotak_picked");
        };
        console.log("reseted");
        txtInfo.innerHTML = "VS";
        if (score_player > score_com){
            resultGame = "Win";
        } else if(score_player < score_com){
            resultGame = "Lose";
        } else {
            resultGame = "Draw";
        }
        await addResultGame("Win");
        score_player = 0;
        score_com = 0;
        txtScoreCom.innerHTML = score_com;
        txtScorePlayer.innerHTML = score_player;
        picked_com = random_suit();
        beepSound(refresh_sound);
    });
}

let tos = (player, com) => {
    let hasil;
    switch (player) {
        case "batu":
            if(com == "gunting"){
                hasil = "win";
            } else if (com == "kertas") {
                hasil = "lose"
            } else{
                hasil = "draw"
            }
            break;
        case "gunting":
            if(com == "gunting"){
                hasil = "draw";
            } else if (com == "kertas") {
                hasil = "win"
            } else{
                hasil = "lose"
            }
            break;
        case "kertas":
            if(com == "gunting"){
                hasil = "lose";
            } else if (com == "kertas") {
                hasil = "draw"
            } else{
                hasil = "win"
            }
            break;
    }
    return hasil
}

let removeSession = () => {
    localStorage.removeItem("accessToken");
    window.location.href = "index.html";
}

let checkUser = () => {
    fetch("http://localhost:8080/api/v1/users/profile",{
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then(res => res.json())
    .then(data => {
        if (data.status == "success") {
            console.log("OK");
        } else {
            window.location.href = "login.html";
        }})
    .catch(err => {
        console.log(err);
    } )
}

let addScore = (result) => {
    dataGame = {
        score : result
    }
    fetch("http://localhost:8080/api/v1/users/games",{
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'POST',
        body: JSON.stringify(dataGame)
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
       })
    .catch(err => {
        console.log(err);
    } )
}

let addResultGame = (result) => {
    dataGame = {
        status : result
    }
    fetch("http://localhost:8080/api/v1/users/logs/create",{
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'POST',
        body: JSON.stringify(dataGame)
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
       })
    .catch(err => {
        console.log(err);
    } )
}

logout.addEventListener("click", function() {
    removeSession()
});

checkUser();
picked();