var i = 1;
let table = document.getElementsByTagName('tbody')[0];
let name = document.querySelector('#name');
let email = document.querySelector('#email');
let password = document.querySelector('#password');
let phone = document.querySelector('#phone');
let address = document.querySelector('#address');
let sex = document.querySelector('#sex');
let idUser = document.getElementById("id_user");
let oldPassword = document.getElementById("old_password");
let btnSubmit = document.getElementById("submit");
let btnToUpdate = document.getElementById("update");

btnToUpdate.classList.add("hide");

btnSubmit.addEventListener('click', () => {
    postData();
});

btnToUpdate.addEventListener('click', () => {
    updateData();
})

let updateData = () => {
    let passwd;

    if (password.value == "") {
        passwd = oldPassword
    } else {
        passwd = password.value
    }

    let data = {
        "name" : name.value,
        "email" : email.value,
        "password" : passwd,
        "phone" : phone.value,
        "address" : address.value,
        "sex" : sex.value
    }

    fetch(`http://localhost:8080/api/v1/users/${idUser.value}`, {
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'PUT',
        body : JSON.stringify(data)
    })
    .then(res => res.text())
    .then(teks => console.log(teks))
    .catch(err => console.log(err));
    location.reload();
}

let getToEdit = (data) => {
    name.value = data.name;
    email.value = data.email;
    phone.value = data.phone;
    address.value = data.address;
    sex.value = data.sex;
    oldPassword.value= data.password;
    idUser.value = data.id_user;
    password.placeholder = "Enter New Password";

    btnToUpdate.classList.remove("hide");
    btnSubmit.classList.add("hide");
}

let getData = (id) => {
    fetch(`http://localhost:8080/api/v1/users/${id}`, {
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then((res) => res.json())
    .then(data => getToEdit(data))
    .catch(err => console.log(err));
}

let postData = () =>
{
    let data = {
        "name" : name.value,
        "email" : email.value,
        "password" : password.value,
        "phone" : phone.value,
        "address" : address.value,
        "sex" : sex.value
    }
    
    fetch('http://localhost:8080/api/v1/users', {
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'POST',
        body : JSON.stringify(data)
    })
    .then(res => res.text())
    .then(teks => console.log(teks))
    .catch(err => console.log(err));

    location.reload();
    alert("sucesss");
}

let deleteData = (id) => {
    fetch(`http://localhost:8080/api/v1/users/${id}`, {
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'DELETE'
    })
    .then(res => res.text())
    .then(teks => console.log(teks))
    .catch(err => console.log(err))

    location.reload();
}

let storeData = (data) => {
    for (let index = 0; index < data.length; index++) {
        var row = document.createElement("tr");
        
        var cell1 = document.createElement("td");
        var cell2= document.createElement("td");
        var cell3 = document.createElement("td");
        var cell4 = document.createElement("td");
        var cell5 = document.createElement("td");
        var cell6 = document.createElement("td");
        var cell7 = document.createElement("td");

        var btnDelete = document.createElement("button");
        var btnUpdate = document.createElement("button");

        btnDelete.classList.add("btn");
        btnUpdate.classList.add("btn");

        btnDelete.classList.add("btn-danger");
        btnUpdate.classList.add("btn-primary");

        btnDelete.innerHTML = "delete";
        btnUpdate.innerHTML = "update";

        btnDelete.addEventListener('click', () => {
            deleteData(data[index].id_user);
        });

        btnUpdate.addEventListener('click', () => {
            getData(data[index].id_user);
        });
        
        cell1.appendChild(document.createTextNode(i++));
        cell2.appendChild(document.createTextNode(data[index].name));
        cell3.appendChild(document.createTextNode(data[index].email));
        cell4.appendChild(document.createTextNode(data[index].phone));
        cell5.appendChild(document.createTextNode(data[index].address));
        cell6.appendChild(document.createTextNode(data[index].sex));
        cell7.appendChild(btnUpdate);
        cell7.appendChild(btnDelete);

        row.appendChild(cell1);
        row.appendChild(cell2);
        row.appendChild(cell3);
        row.appendChild(cell4);
        row.appendChild(cell5);
        row.appendChild(cell6);
        row.appendChild(cell7);

        table.appendChild(row);
    }
}

let fetchData = () => {
    fetch('http://localhost:8080/api/v1/users', {
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then((res) => res.json())
    .then(data => storeData(data))
    .catch(err => console.log(err));
}

let checkAdmin = () => {
    fetch("http://localhost:8080/api/v1/users/profile/admin",{
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        if (data.status !== "success") {
            window.location.href = './../../index.html';
        }
    })
    .catch(err => {
        console.log(err);
    } )
}

checkAdmin();
fetchData();