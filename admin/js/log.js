let tbody = document.getElementsByTagName("tbody")[0];

let storeData = (data) => {
    var i = 1;
    for (let index = 0; index < data.length; index++) {
        var row = document.createElement("tr");
        
        var col1 = document.createElement("td");
        var col2 = document.createElement("td");
        var col3 = document.createElement("td");
        var col4 = document.createElement("td");

        col4.appendChild(document.createTextNode(i++));
        col1.appendChild(document.createTextNode(data[index].id_user))
        col2.appendChild(document.createTextNode(data[index].status))
        col3.appendChild(document.createTextNode(data[index].createdAt))
        
        row.appendChild(col4);
        row.appendChild(col1);
        row.appendChild(col2);
        row.appendChild(col3);

        tbody.appendChild(row);
    }
}

let getData = ( ) => {
    fetch("http://localhost:8080/api/v1/users/logs/all", {
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        storeData(data);
    })
    .catch(err => console.log(err))
}


let checkAdmin = () => {
    fetch("http://localhost:8080/api/v1/users/profile/admin",{
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        if (data.status !== "success") {
            window.location.href = './../../index.html';
        }
    })
    .catch(err => {
        console.log(err);
    } )
}

checkAdmin();
getData();