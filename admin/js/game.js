let btnSubmit = document.getElementById("submit");
let tbody = document.getElementsByTagName("tbody")[0];

btnSubmit.addEventListener("click", () => {
    postData();
})

let postData = () => {
    let dataGame = {
        "id_user" : document.getElementById("id_user").value,
        "score" : document.getElementById("score").value,
        "top_score" : document.getElementById("top_score").value
    }

    console.log(dataGame);

    fetch("http://localhost:8080/api/v1/users/games", {
        headers : {
            "content-type" : "application/json; charset=UTF-8"
        },
        method : 'POST',
        body : JSON.stringify(dataGame)
    })
    .then(res => res.json())
    .then(data => console.log(data))
    .catch(err => console.log(err));

    location.reload();
}

let storeData = (data) => {
    var i = 1;
    for (let index = 0; index < data.length; index++) {
        var row = document.createElement("tr");
        date = data[index].createdAt;

        var col1 = document.createElement("td");
        var col2 = document.createElement("td");
        var col3 = document.createElement("td");
        var col4 = document.createElement("td");
        var col5 = document.createElement("td");

        col1.appendChild(document.createTextNode(i++));
        col2.appendChild(document.createTextNode(data[index].id_user));
        col3.appendChild(document.createTextNode(data[index].score));
        col4.appendChild(document.createTextNode(data[index].top_score));
        col5.appendChild(document.createTextNode(date.substring(0,10)));

        row.appendChild(col1);
        row.appendChild(col2);
        row.appendChild(col3);
        row.appendChild(col4);
        row.appendChild(col5);

        tbody.appendChild(row);
        
    }
}

let getData = () => {
    fetch("http://localhost:8080/api/v1/users/games/all",{
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then((res) => res.json())
    .then(data => storeData(data))
    .catch(err => console.log(err));
}


let checkAdmin = () => {
    fetch("http://localhost:8080/api/v1/users/profile/admin",{
        headers : {
            "content-type" : "application/json; charset=UTF-8",
            "x-access-token" : localStorage.getItem("accessToken")
        },
        method : 'GET',
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        if (data.status !== "success") {
            window.location.href = './../../index.html';
        }
    })
    .catch(err => {
        console.log(err);
    } )
}

checkAdmin();
getData();